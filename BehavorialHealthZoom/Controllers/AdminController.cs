﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BehavorialHealthZoom.Helper;
using BehavorialHealthZoom.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace BehavorialHealthZoom.Controllers
{
    public class AdminController : Controller
    {
        private readonly zoomemailsContext _context;
        private readonly string _user;

        public AdminController(zoomemailsContext context, IHttpContextAccessor httpContextAccessor)
        {
            _context = context;
            _user = httpContextAccessor.HttpContext.User.Identity.Name;
        }

        public IActionResult Index()
        {
            var userName = Authentication.cleanUsername(_user);

            ViewBag.Username = userName;

            var isUserAdmin = false;

            var userAdmin = _context.AdminUsers.Where(a => a.Username == userName).FirstOrDefault();
            if (userAdmin != null)
            {
                isUserAdmin = true;
            }

            ViewBag.IsUserAdmin = isUserAdmin;

            if (!isUserAdmin)
                return RedirectToAction("NotAuthorized", "Home");

            return View();
        }

        [HttpGet]
        public ActionResult GetProviders()
        {
            var jsonData = "";
            try
            {
                var providers = _context.Providers.OrderBy(p => p.DoctorName).ToList();
                jsonData = JsonConvert.SerializeObject(providers);
            }
            catch (Exception)
            {
                jsonData = "";
            }
            var jsonResult = Json(new { data = jsonData });
            return jsonResult;
        }

        [HttpGet]
        public ActionResult GetProviderInfo(int providerId)
        {
            var provider = new Providers();
            try
            {
                provider = _context.Providers.Where(p => p.Id == providerId).FirstOrDefault();
            }
            catch (Exception)
            {
                provider = null;
            }
            return Json(provider);
        }     

        [HttpPost]
        public ActionResult UpdateProvider(IFormCollection collection)
        {
            var providerId = Convert.ToInt32(collection["providerId"].ToString());
            try
            {
                var provider = _context.Providers.Where(p => p.Id == providerId).FirstOrDefault();
                provider.DoctorName = collection["providerName"].ToString();
                provider.ZoomMeetingId = collection["zoomMeetingId"].ToString();
                provider.ModifiedDt = DateTime.Now;
                provider.ModifiedBy = Authentication.cleanUsername(_user);
                _context.SaveChanges();
                return RedirectToAction("Index", "Admin");

            }
            catch (Exception)
            {
                var resultMessage = "There was an issue updating the selected Provider. Please try again.";
                bool resultStatus = false;
                return RedirectToAction("ResultsPage", "Submit", new { resultMessage, resultStatus });
            }           
        }

        [HttpPost]
        public ActionResult RemoveProvider(int providerId)
        {
            string results = null;
            List<string> returnList = new List<string>();
            try
            {
                var provider = _context.Providers.Where(p => p.Id == providerId).FirstOrDefault();
                _context.Providers.Remove(provider);
                _context.SaveChanges();

                results = "success";
            }
            catch (Exception)
            {
                results = "error";
            }
            returnList.Add(results);
            return Json(returnList);
        }

        [HttpPost]
        public ActionResult AddNewProvider(IFormCollection collection)
        {
            try
            {
                var provider = new Providers();
                provider.DoctorName = collection["providerName"].ToString();
                provider.ZoomMeetingId = collection["zoomMeetingId"].ToString();
                provider.CreatedDt = DateTime.Now;
                provider.CreatedBy = Authentication.cleanUsername(_user);
                _context.Providers.Add(provider);
                _context.SaveChanges();

                return RedirectToAction("Index", "Admin");
            }
            catch (Exception)
            {
                var resultMessage = "There was an issue inserying your new Provider. Please try again.";
                bool resultStatus = false;
                return RedirectToAction("ResultsPage", "Submit", new { resultMessage, resultStatus });
            }            
        }

        public IActionResult AdminUsers()
        {
            var userName = Authentication.cleanUsername(_user);

            ViewBag.Username = userName;

            var isUserAdmin = false;

            var userAdmin = _context.AdminUsers.Where(a => a.Username == userName).FirstOrDefault();
            if (userAdmin != null)
            {
                isUserAdmin = true;
            }

            ViewBag.IsUserAdmin = isUserAdmin;

            if (!isUserAdmin)
                return RedirectToAction("NotAuthorized", "Home");

            return View();
        }

        [HttpGet]
        public ActionResult GetAdminUsers()
        {
            var jsonData = "";
            try
            {
                var adminUsers = _context.AdminUsers.OrderBy(p => p.Id).ToList();
                jsonData = JsonConvert.SerializeObject(adminUsers);
            }
            catch (Exception)
            {
                jsonData = "";
            }
            var jsonResult = Json(new { data = jsonData });
            return jsonResult;
        }

        [HttpGet]
        public ActionResult GetAdminUserInfo(int adminId)
        {
            var admin = new AdminUsers();
            try
            {
                admin = _context.AdminUsers.Where(p => p.Id == adminId).FirstOrDefault();
            }
            catch (Exception)
            {
                admin = null;
            }
            return Json(admin);
        }

        [HttpPost]
        public ActionResult UpdateAdminUser(IFormCollection collection)
        {
            var adminId = Convert.ToInt32(collection["adminId"].ToString());
            try
            {
                var adminUser = _context.AdminUsers.Where(p => p.Id == adminId).FirstOrDefault();
                adminUser.Username = collection["adminUserName"].ToString();            
                _context.SaveChanges();
                return RedirectToAction("AdminUsers", "Admin");

            }
            catch (Exception)
            {
                var resultMessage = "There was an issue updating the selected Provider. Please try again.";
                bool resultStatus = false;
                return RedirectToAction("ResultsPage", "Submit", new { resultMessage, resultStatus });
            }
        }

        [HttpPost]
        public ActionResult RemoveAdminUser(int adminId)
        {
            string results = null;
            List<string> returnList = new List<string>();
            try
            {
                var admin = _context.AdminUsers.Where(p => p.Id == adminId).FirstOrDefault();
                _context.AdminUsers.Remove(admin);
                _context.SaveChanges();

                results = "success";
            }
            catch (Exception)
            {
                results = "error";
            }
            returnList.Add(results);
            return Json(returnList);
        }

        [HttpPost]
        public ActionResult AddNewAdminUser(IFormCollection collection)
        {
            try
            {
                var admin = new AdminUsers();
                admin.Username = collection["adminUserName"].ToString();
                admin.CreateDt = DateTime.Now;                
                _context.AdminUsers.Add(admin);
                _context.SaveChanges();

                return RedirectToAction("AdminUsers", "Admin");
            }
            catch (Exception)
            {
                var resultMessage = "There was an issue inserying your new Provider. Please try again.";
                bool resultStatus = false;
                return RedirectToAction("ResultsPage", "Submit", new { resultMessage, resultStatus });
            }
        }
    }
}
