﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using BehavorialHealthZoom.Models;
using Microsoft.AspNetCore.Http;
using BehavorialHealthZoom.Helper;
using Newtonsoft.Json;

namespace BehavorialHealthZoom.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly zoomemailsContext _context;
        private readonly string _user;

        public HomeController(ILogger<HomeController> logger, zoomemailsContext context, IHttpContextAccessor httpContextAccessor)
        {
            _logger = logger;
            _context = context;
            _user = httpContextAccessor.HttpContext.User.Identity.Name;
        }

        public IActionResult Index()
        {
            var userName = Authentication.cleanUsername(_user);

            ViewBag.Username = userName;

            var isUserAdmin = false;

            var userAdmin = _context.AdminUsers.Where(a => a.Username == userName).FirstOrDefault();
            if (userAdmin != null)
            {
                isUserAdmin = true;
            }

            ViewBag.IsUserAdmin = isUserAdmin;

            var providers = _context.Providers.OrderBy(p => p.DoctorName).ToList();
            var apptTypes = _context.AppointmentTypes.ToList();

            var createNewZoomNotification = new CreateNewZoomNotification
            {
                _ProviderList = providers,
                _AppointmentTypes = apptTypes
            };

            return View(createNewZoomNotification);
        }

        public IActionResult Appointments()
        {
            var userName = Authentication.cleanUsername(_user);

            ViewBag.Username = userName;

            var isUserAdmin = false;

            var userAdmin = _context.AdminUsers.Where(a => a.Username == userName).FirstOrDefault();
            if (userAdmin != null)
            {
                isUserAdmin = true;
            }

            ViewBag.IsUserAdmin = isUserAdmin;

            return View();
        }

        public IActionResult NotAuthorized()
        {
            return View();
        }

        [HttpGet]
        public ActionResult GetGeneratedAppointments()
        {
            var jsonData = "";
            var appt_List = new List<GeneratedAppointments>();
            try
            {
                var currentMonth = DateTime.Now.Month;
                var currentDay = DateTime.Now.Day;

                // Gets a list of all of the appointments for this month and at least the current date
                
                var appointments = _context.Appointments.Where(x=> x.AppointmentDt.Month == currentMonth && x.AppointmentDt.Day >= currentDay).ToList();

                foreach (var appt in appointments)
                {
                    var ga = new GeneratedAppointments();

                    var apptType = _context.AppointmentTypes.FirstOrDefault(at => at.Id == appt.AppointmentTypeId);

                    ga.ApptType = apptType.AppointmentType;
                    ga.PatientEmail = appt.PatientEmail;
                    ga.ApptDt = appt.AppointmentDt;

                    var drName = _context.Providers.FirstOrDefault(p => p.Id == appt.ProviderId);

                    if (drName == null)
                        ga.DoctorName = "N/A";                  
                    else
                        ga.DoctorName = drName.DoctorName;

                    ga.CreatedDt = appt.CreateDt;
                    ga.CreatedBy = appt.CreatedBy;

                    appt_List.Add(ga);
                }
                jsonData = JsonConvert.SerializeObject(appt_List);
            }
            catch (Exception)
            {
                jsonData = "";
            }
            var jsonResult = Json(new { data = jsonData });
            return jsonResult;
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
