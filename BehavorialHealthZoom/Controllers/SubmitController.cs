﻿using System;
using System.Linq;
using System.Net;
using System.Net.Mail;
using BehavorialHealthZoom.Helper;
using BehavorialHealthZoom.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace BehavorialHealthZoom.Controllers
{
    public class SubmitController : Controller
    {
        private readonly zoomemailsContext _context;
        private readonly IConfiguration _config;
        private readonly string _user;

        public SubmitController(zoomemailsContext context, IConfiguration configuration, IHttpContextAccessor httpContextAccessor)
        {
            _config = configuration;
            _context = context;
            _user = httpContextAccessor.HttpContext.User.Identity.Name;
        }

        [HttpPost]
        public IActionResult SubmitZoomNotification(IFormCollection collection)
        {
            var userName = Authentication.cleanUsername(_user);

            ViewBag.Username = userName;

            var resultMessage = "";
            bool resultStatus = false;

            var providerId = Convert.ToInt32(collection["provider"].ToString());
            var apptType = Convert.ToInt32(collection["apptType"].ToString());
            var apptDate = Convert.ToDateTime(collection["apptDt"].ToString());
            var patientEmail = collection["patientEmail"].ToString();

            // Get Data for email
            // Get Provider info
            var appointmentType = _context.AppointmentTypes.Where(a => a.Id == apptType).FirstOrDefault();
            var provider = _context.Providers.Where(p => p.Id == providerId).FirstOrDefault();
            var providerZoomMeetingId = provider.ZoomMeetingId;

            var meetingId = providerZoomMeetingId.Substring(0, 3) + " " + providerZoomMeetingId.Substring(3, 3) + " " + providerZoomMeetingId.Substring(6, 4);            

            // Generate Zoom Meeting Link
            var zoomMeetingLink = _config["AppSettings:ZoomMeetingLink"].ToString() + provider.ZoomMeetingId;

            // Get Email Template
            var emailTemplate = _context.EmailTemplate.FirstOrDefault();
            var subject = emailTemplate.Title;

            // Generate Email HTML pieces not from db.
            var introMessage = "";

            if (apptType == 1)
            {
                introMessage = "<p>Hello, </p>" + "<p>Your upcoming appointment with " + provider.DoctorName + ", will be provided via telehealth at Zoom.us. Please be sure that you are in an area with good reception and where you can speak privately with your provider.</p>";               
            }
            else if (apptType == 2)
            {
                introMessage = "<p>Hello, </p>" + "<p>A minor in your household has an upcoming appointment with " + provider.DoctorName + ", which will be provided via telehealth at Zoom.us.Please be sure that you are in an area with good reception and where you can speak privately with your provider. Please share this link or meeting ID with the patient so that they may join the appointment on their own device, if applicable.</p>";               
            }

            var linkMessage = "<p><b>Click this link when you're ready to join: </b><a href=" + zoomMeetingLink + ">" + zoomMeetingLink + "</a></p><p>You can also join from your PC of Phone, with the Zoom Cloud Meeting App, with these credentials: Meeting ID " + meetingId + "</p>";

            var apptInformation = "<p><b><u>Appointment Information</u></b></p>" +
                "<p><b>Appointment With (Provider): </b><i>" + provider.DoctorName + "</i></p>" +
                "<p><b>Appointment Date & Time: </b><i>" + apptDate.ToShortDateString() + " " + apptDate.ToShortTimeString() + "</i></p>" ;

            // If we end up connecting to the app Tammi worked on for sending out reminders
            var sendReminder = Convert.ToBoolean(_config["AppSettings:SendReminder"].ToString());

            // Generate Email components
            var client = new SmtpClient
            {
                DeliveryMethod = SmtpDeliveryMethod.Network,
                Host = _config["AppSettings:EmailSettings:Relay"].ToString(),
                Port = Convert.ToInt32(_config["AppSettings:EmailSettings:NetworkCredentials:Port"].ToString()),
                EnableSsl = false
            };

            var networkUsername = _config["AppSettings:EmailSettings:NetworkCredentials:Username"].ToString();
            var networkPassword = _config["AppSettings:EmailSettings:NetworkCredentials:Password"].ToString();
            var credential = new NetworkCredential(networkUsername, networkPassword);
            client.Credentials = credential;

            if (!sendReminder)
            {
                try
                {
                    var filePath = _config["AppSettings:FilePath"].ToString() + _config["AppSettings:FileName"].ToString();
                    var filePath2 = _config["AppSettings:FilePath"].ToString() + _config["AppSettings:FileName2"].ToString();

                    var m = new MailMessage();
                    m.From = new MailAddress(_config["AppSettings:EmailSettings:BehavioralHealthEmail"].ToString());
                    m.To.Add(patientEmail);
                    m.Subject = subject;
                    m.Body = introMessage + apptInformation + emailTemplate.EmailBodyHtml + linkMessage + emailTemplate.EmailFooterHtml;
                    m.Attachments.Add(new Attachment(filePath));
                    m.Attachments.Add(new Attachment(filePath2));
                    m.IsBodyHtml = true;

                    try
                    {
                        client.Send(m);
                        resultMessage = "You have successfully sent a notification to patient email: " + patientEmail + ".";
                        resultStatus = true;

                        // Save new Appointment
                        var newAppt = new Appointments()
                        {
                            Id = Guid.NewGuid(),
                            ProviderId = providerId,
                            AppointmentTypeId = apptType,
                            AppointmentDt = apptDate,
                            PatientEmail = patientEmail,
                            CreateDt = DateTime.Now,
                            CreatedBy = userName
                        };
                        _context.Appointments.Add(newAppt);
                        _context.SaveChanges();
                    }
                    catch (SmtpFailedRecipientException)
                    {
                        resultMessage = "There was an issue sending notification to patient email.";
                    }                    
                }
                catch (Exception)
                {
                    resultMessage = "There was an issue sending notification to patient email.";
                }
            }
            return RedirectToAction("ResultsPage", "Submit", new { resultMessage, resultStatus });
        }

        public IActionResult ResultsPage(string resultMessage, bool resultStatus)
        {
            var userName = Authentication.cleanUsername(_user);

            ViewBag.Username = userName;

            var isUserAdmin = false;

            var userAdmin = _context.AdminUsers.Where(a => a.Username == userName).FirstOrDefault();
            if (userAdmin != null)
            {
                isUserAdmin = true;
            }

            ViewBag.IsUserAdmin = isUserAdmin;

            var r = new Results();
            r.ResultMessage = resultMessage;
            r.ResultStatus = resultStatus;
            return View(r);
        }
    }
}
