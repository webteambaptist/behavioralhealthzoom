﻿using System;
using System.Collections.Generic;

namespace BehavorialHealthZoom.Models
{
    public partial class AdminUsers
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public DateTime CreateDt { get; set; }
    }
}
