﻿using System;
using System.Collections.Generic;

namespace BehavorialHealthZoom.Models
{
    public partial class AppointmentTypes
    {
        public int Id { get; set; }
        public string AppointmentType { get; set; }
    }
}
