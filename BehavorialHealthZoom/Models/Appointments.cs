﻿using System;
using System.Collections.Generic;

namespace BehavorialHealthZoom.Models
{
    public partial class Appointments
    {
        public Guid Id { get; set; }
        public int ProviderId { get; set; }
        public int AppointmentTypeId { get; set; }
        public DateTime AppointmentDt { get; set; }
        public string PatientEmail { get; set; }
        public DateTime CreateDt { get; set; }
        public string CreatedBy { get; set; }
    }

    public class GeneratedAppointments
    {
        public string ApptType { get; set; }
        public string PatientEmail { get; set; }
        public DateTime ApptDt  { get; set; }
        public string DoctorName { get; set; }
        public DateTime CreatedDt { get; set; }
        public string CreatedBy { get; set; }
    }
}
