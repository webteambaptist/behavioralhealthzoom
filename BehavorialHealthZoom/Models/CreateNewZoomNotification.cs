﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BehavorialHealthZoom.Models
{
    public class CreateNewZoomNotification
    {
        public List<Providers> _ProviderList { get; set; }
        public List<AppointmentTypes> _AppointmentTypes { get; set; }
    }
}
