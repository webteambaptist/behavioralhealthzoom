﻿using System;
using System.Collections.Generic;

namespace BehavorialHealthZoom.Models
{
    public partial class EmailTemplate
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string EmailBodyHtml { get; set; }
        public string EmailFooterHtml { get; set; }
        public DateTime CreateDt { get; set; }
        public string CreatedBy { get; set; }
    }
}
