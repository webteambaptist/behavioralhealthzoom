﻿using System;
using System.Collections.Generic;

namespace BehavorialHealthZoom.Models
{
    public partial class OfficeConfirmations
    {
        public int Id { get; set; }
        public string MeetingId { get; set; }
        public string OfficeEmail { get; set; }
        public bool ReminderConfirmation { get; set; }
        public bool CancelConfirmation { get; set; }
        public bool KickBackDateFormat { get; set; }
    }
}
