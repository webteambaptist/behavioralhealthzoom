﻿using System;
using System.Collections.Generic;

namespace BehavorialHealthZoom.Models
{
    public partial class Providers
    {
        public int Id { get; set; }
        public string DoctorName { get; set; }
        public string ZoomMeetingId { get; set; }
        public DateTime CreatedDt { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? ModifiedDt { get; set; }
        public string ModifiedBy { get; set; }
    }
}
