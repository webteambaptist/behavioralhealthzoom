﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BehavorialHealthZoom.Models
{
    public class Results
    {
        public string ResultMessage { get; set; }
        public bool ResultStatus { get; set; }
    }
}
