﻿using System;
using System.Collections.Generic;

namespace BehavorialHealthZoom.Models
{
    public partial class ZoomMeeting
    {
        public int Id { get; set; }
        public string PatientEmail { get; set; }
        public string FromEmail { get; set; }
        public string DoctorsName { get; set; }
        public string FormattedDrName { get; set; }
        public string MeetingId { get; set; }
        public string Password { get; set; }
        public string ZoomUrl { get; set; }
        public DateTime EntryDate { get; set; }
        public DateTime MeetingDateTime { get; set; }
        public bool Cancelled { get; set; }
        public DateTime? DateTimeCancelled { get; set; }
        public bool ReminderSent { get; set; }
        public DateTime? DateTimeReminderSent { get; set; }
        public bool PastTime { get; set; }
        public DateTime? PastTimeEntryDate { get; set; }
    }
}
