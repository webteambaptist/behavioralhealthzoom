﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace BehavorialHealthZoom.Models
{
    public partial class zoomemailsContext : DbContext
    {
        public zoomemailsContext()
        {
        }

        public zoomemailsContext(DbContextOptions<zoomemailsContext> options)
            : base(options)
        {
        }

        public virtual DbSet<AdminUsers> AdminUsers { get; set; }
        public virtual DbSet<AppointmentTypes> AppointmentTypes { get; set; }
        public virtual DbSet<Appointments> Appointments { get; set; }
        public virtual DbSet<EmailTemplate> EmailTemplate { get; set; }
        public virtual DbSet<OfficeConfirmations> OfficeConfirmations { get; set; }
        public virtual DbSet<Providers> Providers { get; set; }
        public virtual DbSet<ZoomMeeting> ZoomMeeting { get; set; }        

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AdminUsers>(entity =>
            {
                entity.ToTable("AdminUsers", "dbo");

                entity.Property(e => e.CreateDt).HasColumnType("datetime");

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<AppointmentTypes>(entity =>
            {
                entity.ToTable("AppointmentTypes", "dbo");

                entity.Property(e => e.AppointmentType)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Appointments>(entity =>
            {
                entity.ToTable("Appointments", "dbo");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.AppointmentDt).HasColumnType("datetime");

                entity.Property(e => e.CreateDt).HasColumnType("datetime");

                entity.Property(e => e.CreatedBy).HasMaxLength(100);

                entity.Property(e => e.PatientEmail)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<EmailTemplate>(entity =>
            {
                entity.ToTable("EmailTemplate", "dbo");

                entity.Property(e => e.CreateDt).HasColumnType("datetime");

                entity.Property(e => e.CreatedBy).HasMaxLength(100);

                entity.Property(e => e.EmailBodyHtml)
                    .IsRequired()
                    .HasColumnName("EmailBodyHTML")
                    .HasColumnType("ntext");

                entity.Property(e => e.EmailFooterHtml)
                    .IsRequired()
                    .HasColumnName("EmailFooterHTML")
                    .HasColumnType("ntext");

                entity.Property(e => e.Title)
                    .IsRequired()
                    .HasMaxLength(250);
            });

            modelBuilder.Entity<OfficeConfirmations>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("OfficeConfirmations", "dbo");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.MeetingId)
                    .IsRequired()
                    .HasColumnName("MeetingID")
                    .HasMaxLength(50);

                entity.Property(e => e.OfficeEmail).IsRequired();
            });

            modelBuilder.Entity<Providers>(entity =>
            {
                entity.ToTable("Providers", "dbo");

                entity.Property(e => e.CreatedBy).HasMaxLength(100);

                entity.Property(e => e.CreatedDt).HasColumnType("datetime");

                entity.Property(e => e.DoctorName)
                    .IsRequired()
                    .HasMaxLength(500);

                entity.Property(e => e.ModifiedBy).HasMaxLength(100);

                entity.Property(e => e.ModifiedDt).HasColumnType("datetime");

                entity.Property(e => e.ZoomMeetingId)
                    .HasColumnName("ZoomMeetingID")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<ZoomMeeting>(entity =>
            {
                entity.ToTable("ZoomMeeting", "dbo");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.DateTimeCancelled).HasColumnType("datetime");

                entity.Property(e => e.DateTimeReminderSent).HasColumnType("datetime");

                entity.Property(e => e.DoctorsName).IsRequired();

                entity.Property(e => e.EntryDate).HasColumnType("datetime");

                entity.Property(e => e.FormattedDrName).IsRequired();

                entity.Property(e => e.FromEmail).IsRequired();

                entity.Property(e => e.MeetingDateTime).HasColumnType("datetime");

                entity.Property(e => e.MeetingId).IsRequired();

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.PastTimeEntryDate).HasColumnType("datetime");

                entity.Property(e => e.PatientEmail).IsRequired();

                entity.Property(e => e.ZoomUrl).IsRequired();
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
