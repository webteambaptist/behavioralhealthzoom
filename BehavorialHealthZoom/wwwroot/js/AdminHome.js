﻿var providerTable;

$(document).ready(function () {
    $('#updateInfo').hide();
    $('#addNew').hide();
    GenerateProviderTable();
});

$("#searchbox").keyup(function () {
    providerTable.rows().search(this.value).draw();
});

function GenerateProviderTable() {
    providerTable = $('#ProviderList').DataTable({
        "dom": '<"top"l>rt<"bottom"ip><"clear">',
        "destroy": true,
        "bAutoWidth": true,
        "ajax": {
            "url": "Admin/GetProviders",
            "error": function (errResp) {
                alert("There was an issue processing your request. Please try again.");
            },
            "dataSrc": function (json) {
                var data = JSON.parse(json.data);
                return data;
            },
            "type": "GET",
            "datatype": "json"
        },
        "language": {
            "emptyTable": "There are currently no Providers."
        },
        "columns": [
            {
                "data": 'Id', sDefaultContent: '', "render": function (name, type, data, meta) {
                    var id = data.Id;
                    return "<a id='updateProvider' title='Modify Provider Information' href='#' onclick='GetProviderInfo(" + id + ")'><i class='fas fa-edit' style='font-size: 1.5em !important;'></i></a>";
                }
            },
            { "data": 'DoctorName', sDefaultContent: '' },
            { "data": 'ZoomMeetingId', sDefaultContent: '' },
            {
                "data": 'CreatedDt', sDefaultContent: '', "render": function (name, type, data, meta) {
                    var date = new Date(data.CreatedDt);
                    var month = date.getMonth() + 1;
                    var day = date.getDate();
                    var year = date.getFullYear();
                    var hour = date.getHours();
                    var minutes = date.getMinutes();
                    var ampm = (hour >= 12) ? "PM" : "AM";

                    var _ESThour = hour;
                    if (_ESThour >= 12) {
                        _ESThour = hour - 12;
                    }
                    if (_ESThour == 0) {
                        _ESThour = 12;
                    }
                    hour = _ESThour;
                    hour = hour < 10 ? "0" + hour : hour;
                    minutes = minutes < 10 ? "0" + minutes : minutes;
                    month = month < 10 ? "0" + month : month;
                    day = day < 10 ? "0" + day : day;
                    return month + "/" + day + "/" + year + " " + hour + ":" + minutes + " " + ampm;
                }
            },
            {
                "data": 'Id', sDefaultContent: '', "render": function (name, type, data, meta) {
                    var id = data.Id;
                    return "<a id='removeProvider' title='Remove Provider' href='#' onclick='RemoveProvider(" + id + ")'><i class='far fa-trash-alt' style='color:red !important; font-size: 1.5em !important;'></i></a>";
                }
            },
        ],
        "columnDefs": [
            {
                targets: 0,
                className: 'text-center'
            },
            {
                targets: 1,
                className: 'text-center'
            },
            {
                targets: 2,
                className: 'text-center'
            },
            {
                targets: 3,
                className: 'text-center'
            },
            {
                targets: 4,
                className: 'text-center'
            }
        ],
        "pageLength": 10,
        "pagingType": "full_numbers",
        responsive: true
    });
}

function GetProviderInfo(id) {
    $('#addNew').hide();
    $.ajax({
        type: "GET",
        url: "Admin/GetProviderInfo",
        data: { ProviderId: id },
        dataType: "json",
        success: function (response) {
            if (response != null) {
                $('#updateInfo').show();
                $('#providerId').val(response.id);
                $('#providerName').val(response.doctorName);
                $('#zoomMeetingId').val(response.zoomMeetingId);
            }
        }
    });
}

function RemoveProvider(id) {
    var results = confirm('Are you sure you want to remove this Provider?');

    if (results) {
        $.ajax({
            type: "POST",
            url: "Admin/RemoveProvider",
            data: { ProviderId: id },
            dataType: "json",
            success: function (response) {
                if (response != null && response == 'success') {
                    alert('Provider Removed');
                    GenerateProviderTable();
                    $('#updateInfo').hide();
                    $('#addNew').hide();
                }
            }
        });
    }
}

$('#btnAddNewProvider').click(function () {
    $('#updateInfo').hide();
    $('#addNew').show();
});
