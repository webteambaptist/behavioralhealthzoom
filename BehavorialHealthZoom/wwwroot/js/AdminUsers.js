﻿var adminUserTable;

$(document).ready(function () {
    $('#updateInfo').hide();
    $('#addNew').hide();
    GenerateAdminUserTable();
});

$("#searchbox").keyup(function () {
    adminUserTable.rows().search(this.value).draw();
});

function GenerateAdminUserTable() {
    adminUserTable = $('#AdminUserList').DataTable({
        "dom": '<"top"l>rt<"bottom"ip><"clear">',
        "destroy": true,
        "bAutoWidth": true,
        "ajax": {
            "url": "../Admin/GetAdminUsers",
            "error": function (errResp) {
                alert("There was an issue processing your request. Please try again.");
            },
            "dataSrc": function (json) {
                var data = JSON.parse(json.data);
                return data;
            },
            "type": "GET",
            "datatype": "json"
        },
        "language": {
            "emptyTable": "There are currently no Admin Users."
        },
        "columns": [
            {
                "data": 'Id', sDefaultContent: '', "render": function (name, type, data, meta) {
                    var id = data.Id;
                    return "<a id='updateProvider' title='Modify Admin User Information' href='#' onclick='GetAdminUserInfo(" + id + ")'><i class='fas fa-edit' style='font-size: 1.5em !important;'></i></a>";
                }
            },
            { "data": 'Username', sDefaultContent: '' },            
            {
                "data": 'Id', sDefaultContent: '', "render": function (name, type, data, meta) {
                    var id = data.Id;
                    return "<a id='removeProvider' title='Remove Admin User' href='#' onclick='RemoveAdminUser(" + id + ")'><i class='far fa-trash-alt' style='color:red !important; font-size: 1.5em !important;'></i></a>";
                }
            },
        ],
        "columnDefs": [
            {
                targets: 0,
                className: 'text-center'
            },
            {
                targets: 1,
                className: 'text-center'
            },
            {
                targets: 2,
                className: 'text-center'
            }         
        ],
        "pageLength": 10,
        "pagingType": "full_numbers",
        responsive: true
    });
}

function GetAdminUserInfo(id) {
    $('#addNew').hide();
    $.ajax({
        type: "GET",
        url: "../Admin/GetAdminUserInfo",
        data: { AdminId: id },
        dataType: "json",
        success: function (response) {
            if (response != null) {
                $('#updateInfo').show();
                $('#adminId').val(response.id);
                $('#adminUserName').val(response.username);
            }
        }
    });
}

function RemoveAdminUser(id) {
    var results = confirm('Are you sure you want to remove this Admin User?');

    if (results) {
        $.ajax({
            type: "POST",
            url: "../Admin/RemoveAdminUser",
            data: { AdminId: id },
            dataType: "json",
            success: function (response) {
                if (response != null && response == 'success') {
                    alert('Admin User Removed');
                    GenerateAdminUserTable();
                    $('#updateInfo').hide();
                    $('#addNew').hide();
                }
            }
        });
    }
}

$('#btnAddNewAdminUser').click(function () {
    $('#updateInfo').hide();
    $('#addNew').show();
});
