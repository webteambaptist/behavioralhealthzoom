﻿var apptTable;

$(document).ready(function () {
    GenerateAppointmentTable();
});

$("#searchbox").keyup(function () {
    apptTable.rows().search(this.value).draw();
});

function GenerateAppointmentTable() {
    apptTable = $('#AppointmentList').DataTable({
        "dom": '<"top"l>rt<"bottom"ip><"clear">',
        "destroy": true,
        "bAutoWidth": true,
        "ajax": {
            "url": "../Home/GetGeneratedAppointments",
            "error": function (errResp) {
                alert("There was an issue processing your request. Please try again.");
            },
            "dataSrc": function (json) {
                var data = JSON.parse(json.data);
                return data;
            },
            "type": "GET",
            "datatype": "json"
        },
        "language": {
            "emptyTable": "There are currently no Generated Appointments at this time."
        },
        "columns": [
            { "data": 'ApptType', sDefaultContent: '' },
            { "data": 'PatientEmail', sDefaultContent: '' },
            {
                "data": 'ApptDt', sDefaultContent: '', "render": function (name, type, data, meta) {
                    var date = new Date(data.ApptDt);
                    var month = date.getMonth() + 1;
                    var day = date.getDate();
                    var year = date.getFullYear();
                    var hour = date.getHours();
                    var minutes = date.getMinutes();
                    var ampm = (hour >= 12) ? "PM" : "AM";

                    var _ESThour = hour;
                    if (_ESThour >= 12) {
                        _ESThour = hour - 12;
                    }
                    if (_ESThour == 0) {
                        _ESThour = 12;
                    }
                    hour = _ESThour;
                    hour = hour < 10 ? "0" + hour : hour;
                    minutes = minutes < 10 ? "0" + minutes : minutes;
                    month = month < 10 ? "0" + month : month;
                    day = day < 10 ? "0" + day : day;
                    return month + "/" + day + "/" + year + " " + hour + ":" + minutes + " " + ampm;
                }
            },
            { "data": 'DoctorName', sDefaultContent: '' },
            {
                "data": 'CreatedDt', sDefaultContent: '', "render": function (name, type, data, meta) {
                    var date = new Date(data.CreatedDt);
                    var month = date.getMonth() + 1;
                    var day = date.getDate();
                    var year = date.getFullYear();
                    var hour = date.getHours();
                    var minutes = date.getMinutes();
                    var ampm = (hour >= 12) ? "PM" : "AM";

                    var _ESThour = hour;
                    if (_ESThour >= 12) {
                        _ESThour = hour - 12;
                    }
                    if (_ESThour == 0) {
                        _ESThour = 12;
                    }
                    hour = _ESThour;
                    hour = hour < 10 ? "0" + hour : hour;
                    minutes = minutes < 10 ? "0" + minutes : minutes;
                    month = month < 10 ? "0" + month : month;
                    day = day < 10 ? "0" + day : day;
                    return month + "/" + day + "/" + year + " " + hour + ":" + minutes + " " + ampm;
                }
            },
            { "data": 'CreatedBy', sDefaultContent: '' }
        ],
        "columnDefs": [
            {
                targets: 0,
                className: 'text-center'
            },
            {
                targets: 1,
                className: 'text-center'
            },
            {
                targets: 2,
                className: 'text-center'
            },
            {
                targets: 3,
                className: 'text-center'
            },
            {
                targets: 4,
                className: 'text-center'
            },
            {
                targets: 5,
                className: 'text-center'
            }
        ],
        "pageLength": 10,
        "pagingType": "full_numbers",
        "order": [[2, "desc"]],
        responsive: true
    });
}
