﻿
$(document).ready(function () {
    $('#datetimepicker1').datetimepicker();

    $("#Adult").prop("checked", true);
});

function checkPastTime(inputDateTime) {

    var inputDt = new Date(inputDateTime.value);
    var currentDt = new Date();

    var month = inputDt.getMonth() + 1;
    var day = inputDt.getDate();
    var year = inputDt.getFullYear();
    month = month < 10 ? "0" + month : month;
    day = day < 10 ? "0" + day : day;

    var hour = inputDt.getHours();
    var minutes = inputDt.getMinutes();
    minutes = minutes < 10 ? "0" + minutes : minutes;

    var ampm = (hour >= 12) ? "PM" : "AM";
    var myDate = month + "/" + day + "/" + year + " " + hour + ":" + minutes + " " + ampm;

    //checks year
    if (inputDt.getFullYear() < currentDt.getFullYear()) {
        $('#datetimepicker1').val('');
        alert("You selected " + myDate + ". Sorry, this date is invalid! Please select a current/upcoming year.");
    }

    //checks month
    else if ((inputDt.getFullYear() == currentDt.getFullYear()) && (inputDt.getMonth() < currentDt.getMonth())) {
        $('#datetimepicker1').val('');
        alert("You selected " + myDate + ". Sorry, this date is invalid! Please select a current/upcoming month.");
    }

        //check day
    else if ((inputDt.getFullYear() == currentDt.getFullYear()) && (inputDt.getMonth() == currentDt.getMonth()) && (inputDt.getDate() < currentDt.getDate())) {
        $('#datetimepicker1').val('');
        alert("You selected " + myDate + ". Sorry, this date is invalid!! Please select a current/upcoming date.");
    }

    //check hour
    //if ((inputDt.getDate() == currentDt.getDate()) && (inputDt.getHours() < currentDt.getHours())) {
    //    $('#datetimepicker1').val('');
    //    alert("You selected " + myDate + ". Sorry, this date is invalid! Please select a current/upcoming hour for the time.");
    //}  
    //else if ((inputDt.getDate() == currentDt.getDate()) && (inputDt.getHours() == currentDt.getHours()) && (inputDt.getMinutes() < currentDt.getMinutes())) {
    //    $('#datetimepicker1').val('');
    //    alert("You selected " + myDate + ". Sorry, this date is invalid! Please select a current/upcoming minute for the time.");
    //}
}